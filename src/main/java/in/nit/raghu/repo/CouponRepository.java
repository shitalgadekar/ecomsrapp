package in.nit.raghu.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import in.nit.raghu.entity.Coupon;

public interface CouponRepository extends JpaRepository<Coupon,Long> {

}
