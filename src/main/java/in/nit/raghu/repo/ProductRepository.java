package in.nit.raghu.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.nit.raghu.entity.Product;


public interface ProductRepository extends JpaRepository<Product, Long> {

	@Query("SELECT id,name FROM Product")
	List<Object[]> getProductIdAndNames();
}
