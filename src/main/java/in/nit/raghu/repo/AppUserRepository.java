package in.nit.raghu.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import in.nit.raghu.entity.AppUser;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {

}
