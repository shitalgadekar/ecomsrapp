package in.nit.raghu.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import in.nit.raghu.entity.Shipping;

public interface ShippingRepository extends JpaRepository<Shipping ,Long>{

}
