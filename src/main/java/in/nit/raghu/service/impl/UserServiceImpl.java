package in.nit.raghu.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nit.raghu.constants.UserStatus;
import in.nit.raghu.entity.User;
import in.nit.raghu.repo.UserRepository;
import in.nit.raghu.service.IUserService;
import in.nit.raghu.util.AppUtil;

@Service
public class UserServiceImpl implements IUserService {
	
	@Autowired
	private UserRepository repo;
	
	@Override
	public Long saveUser(User user) {
		// TODO : PASSWORD ENCODING
		user.setStatus(UserStatus.INACTIVE.name());
		user.setPassword(AppUtil.genPwd());
		return repo.save(user).getId();
		// TODO : SENDIG EMAIL
	}

	@Override
	public Optional<User> findByEmail(String email) {
		return repo.findByEmail(email);
	}

	@Override
	public List<User> getAllUsers() {
		return repo.findAll();
	}

}
