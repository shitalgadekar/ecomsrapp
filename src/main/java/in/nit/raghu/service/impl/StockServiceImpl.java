package in.nit.raghu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import in.nit.raghu.entity.Stock;
import in.nit.raghu.repo.StockRepository;
import in.nit.raghu.service.IStockService;

@Service
public class StockServiceImpl implements IStockService{

	@Autowired
	private StockRepository repo;
	
	@Override
	public Long createStock(Stock stock) {
		return repo.save(stock).getId();
	}

	@Override
	@Transactional
	public void updateStock(Long id, Long count) {
		repo.updateStock(id, count);
	}

	@Override
	public List<Stock> getStockDetails() {
		return repo.findAll();
	}
    
	//integration
	public Long getStockIdByProduct(Long productId) {
		return repo.getStockIdByProduct(productId);
	}


}
