package in.nit.raghu.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import in.nit.raghu.entity.Brand;
import in.nit.raghu.entity.Coupon;
import in.nit.raghu.repo.CouponRepository;
import in.nit.raghu.service.ICouponService;

@Service
public class CouponServiceImpl implements ICouponService{

	@Autowired
	private CouponRepository repo;
	
	@Override
	@Transactional
	public Long saveCoupon(Coupon coupon) {
		 return repo.save(coupon).getId();
	}

	
	@Override
	@Transactional
	public void updateCoupon(Coupon coupon) {
		repo.save(coupon);
	}

	@Override
	@Transactional
	public void deleteCoupon(Long id) {
		repo.deleteById(id);
	}

	@Override
	@Transactional(readOnly=true)
	public Coupon getOneCoupon(Long id) {
		Optional<Coupon> opt=repo.findById(id);
		  if(opt.isPresent()) {
			  return opt.get();	  }
		  return null;
	}

	@Override
	@Transactional(readOnly=true)
	public List<Coupon> getAllCoupons() {
		return repo.findAll();
	}

}
