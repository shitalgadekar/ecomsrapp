package in.nit.raghu.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import in.nit.raghu.entity.Brand;
import in.nit.raghu.repo.BrandRepository;
import in.nit.raghu.service.IBrandService;
import in.nit.raghu.util.AppUtil;

@Service
public class BrandServiceImpl implements IBrandService {
  @Autowired
  private BrandRepository repo;

  @Override
  @Transactional
  public Long saveBrand(Brand brand) {
    return repo.save(brand).getId();
  }

  @Override
  @Transactional
  public void updateBrand(Brand brand) {
    repo.save(brand);
  }

  @Override
  @Transactional
  public void deleteBrand(Long id) {
    repo.deleteById(id);
  }

  @Override
  @Transactional( readOnly = true )
  public Brand getOneBrand(Long id) {
	  Optional<Brand> opt=repo.findById(id);
	  if(opt.isPresent()) {
		  return opt.get();	  }
	  return null;
  }

  @Override
  @Transactional( readOnly = true)
  public List<Brand> getAllBrands() {
    return repo.findAll();
  }

//integration
   @Override
   public Map<Integer, String> getBrandIdAndName() {
	   List<Object[]>  list = repo.getBrandIdAndName();
		return AppUtil.convertListToMap(list);

   }
}
