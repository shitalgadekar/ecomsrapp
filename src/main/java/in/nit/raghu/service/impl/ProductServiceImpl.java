package in.nit.raghu.service.impl;

import in.nit.raghu.entity.Product;
import in.nit.raghu.exception.ProductNotFoundException;
import in.nit.raghu.repo.ProductRepository;
import in.nit.raghu.service.IProductService;
import in.nit.raghu.util.AppUtil;

import java.lang.Long;
import java.lang.Override;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author:RAGHU SIR 
 *  Generated F/w:SHWR-Framework 
 */
@Service
public class ProductServiceImpl implements IProductService {
	@Autowired
	private ProductRepository repo;

	public Long saveProduct(Product p) {
		return repo.save(p).getId();
	}

	public void updateProduct(Product p) {
		if(p.getId()==null || !repo.existsById(p.getId()))
			throw new ProductNotFoundException("Product Not Found");
		else
			repo.save(p);
	}

	public void deleteProduct(Long id) {
		repo.delete(getOneProduct(id));
	}

	public Product getOneProduct(Long id) {
		return repo.findById(id)
				.orElseThrow(()->new ProductNotFoundException("Product Not Found"));
	}

	public List<Product> getAllProducts() {
		return repo.findAll();
	}

	@Override
	public Map<Long, String> getProductIdAndName() {
		List<Object[]> list = repo.getProductIdAndNames();
		return AppUtil.convertListToMapLong(list);
	}

	

}
