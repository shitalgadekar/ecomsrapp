package in.nit.raghu.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import in.nit.raghu.entity.Category;
import in.nit.raghu.repo.CategoryRepository;
import in.nit.raghu.service.ICategoryService;
import in.nit.raghu.util.AppUtil;

@Service
public class CategoryServiceImpl implements ICategoryService {
	
	@Autowired
	private CategoryRepository repo;

	@Override
	@Transactional
	public Long saveCategory(Category category) {
		return repo.save(category).getId();
	}

	@Override
	@Transactional
	public void updateCatgory(Category category) {
		repo.save(category);
	}

	@Override
	@Transactional
	public void deleteCategory(Long id) {
		repo.deleteById(id);
	}

	@Override
	@Transactional(readOnly=true)
	public Category getOneCategory(Long id) {
		return repo.findById(id).get();
	}

	@Override
	@Transactional(readOnly=true)
	public List<Category> getAllCategorys() {
		return repo.findAll();
	}
    
	//integration
	@Override
	public Map<Long, String> getCategoryIdAndName(String status) {
		List<Object[]> list = repo.getCategoryIdAndName(status);
		return AppUtil.convertListToMapLong(list);
	}


}
