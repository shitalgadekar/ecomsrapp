package in.nit.raghu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nit.raghu.entity.AppUser;
import in.nit.raghu.repo.AppUserRepository;
import in.nit.raghu.service.IAppUserService;

@Service
public class AppUserServiceImpl implements IAppUserService {

	@Autowired
	private AppUserRepository repo;
	
	@Override
	public Long saveAppUser(AppUser user) {
		// TODO Auto-generated method stub
		return repo.save(user).getId();
	}

	@Override
	public List<AppUser> getAllAppUser() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

}
