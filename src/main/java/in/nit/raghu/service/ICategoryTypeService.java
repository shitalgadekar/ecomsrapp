package in.nit.raghu.service;

import java.util.List;
import java.util.Map;

import in.nit.raghu.entity.CategoryType;

public interface ICategoryTypeService {
	public Long saveCategoryType(CategoryType categorytype);
	public void updateCategoryType(CategoryType categorytype);
	public void deleteCategoryType(Long id);
	public CategoryType getOneCategoryType(Long id);
	public List<CategoryType> getAllCategoryTypes();

	//for category module integration
	Map<Integer,String> getCategoryTypeIdAndName();

}
