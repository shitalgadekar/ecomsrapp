package in.nit.raghu.service;

import java.util.List;

import in.nit.raghu.entity.Coupon;

public interface ICouponService {
     public Long saveCoupon(Coupon coupon);
     public void updateCoupon(Coupon coupon);
     public void deleteCoupon(Long id);
     public Coupon getOneCoupon(Long id);
     public List<Coupon> getAllCoupons();
}
