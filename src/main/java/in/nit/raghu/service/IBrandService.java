package in.nit.raghu.service;

import java.util.List;
import java.util.Map;

import in.nit.raghu.entity.Brand;

public interface IBrandService {
	Long saveBrand(Brand brand);

	void updateBrand(Brand brand);

	void deleteBrand(Long id);

	Brand getOneBrand(Long id);

	List<Brand> getAllBrands();
	
    //for integration
	Map<Integer,String> getBrandIdAndName();
}
