package in.nit.raghu.service;

import java.util.List;

import in.nit.raghu.entity.Stock;

public interface IStockService {
   Long createStock(Stock stock);
   void updateStock(Long id, Long count);
   List <Stock> getStockDetails();
   
   Long getStockIdByProduct(Long productId);
}
