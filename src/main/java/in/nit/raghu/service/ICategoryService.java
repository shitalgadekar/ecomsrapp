package in.nit.raghu.service;

import java.util.List;
import java.util.Map;

import in.nit.raghu.entity.Category;

public interface ICategoryService {
   public Long saveCategory(Category category);
   public void updateCatgory(Category category);
   public void deleteCategory(Long id);
   public Category getOneCategory(Long id);
   public List<Category> getAllCategorys();
   //for integration
   Map<Long,String> getCategoryIdAndName(String status);
}
