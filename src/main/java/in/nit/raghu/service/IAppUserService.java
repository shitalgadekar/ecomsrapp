package in.nit.raghu.service;

import java.util.List;

import in.nit.raghu.entity.AppUser;

public interface IAppUserService {

	Long saveAppUser(AppUser user);
	List<AppUser> getAllAppUser();
}
