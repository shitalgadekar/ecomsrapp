package in.nit.raghu.service;

import java.util.List;
import java.util.Map;

import in.nit.raghu.entity.Product;

public interface IProductService {
	Long saveProduct(Product product);

	void updateProduct(Product product);

	void deleteProduct(Long id);

	Product getOneProduct(Long id);

	List<Product> getAllProducts();

	//integration in stock
	Map<Long,String> getProductIdAndName();
}
