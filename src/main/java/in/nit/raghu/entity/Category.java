package in.nit.raghu.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Table(name="category_tab")
@AllArgsConstructor
@NoArgsConstructor
public class Category {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
	private Long id;
    
    @Column(name="categoryname")
	private String name;
    
	@Column(name="categoryalias")
	private String alias;
	
	/*
	 * @Column(name="categoryctype") 
	 * private String ctype;
	 */
	
	@Column(name="categorystatus")
	private String status;
	
	@Column(name="categorynote")
	private String note;
	
	//module integrations
	@ManyToOne
	@JoinColumn(name="categorytype_fk")
	private CategoryType categoryType;

}
