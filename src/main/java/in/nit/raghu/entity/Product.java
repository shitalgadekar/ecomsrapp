package in.nit.raghu.entity;

import java.lang.Long;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "product_tab")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name="productname")
	private String name;
	
	@Column (name="shortDescription")
	private String shortDesc;
	
	@Column (name="fullDescription")
	private String fullDesc;
	
	@Column (name="status")
	private String status;
	
	@Column(name="instock")
	private String inStock;
	
	@Column(name="productcost")
	private Double cost;
	
	@Column(name="length")
	private Double length;
	
	@Column(name="width")
	private Double width;
	
	@Column(name="height")
	private Double height;
	
	@Column(name="dimtype")
	private String dimType;
	
	@Column(name="weight")
	private Double weight;
	
	@Column(name="wttype")
	private String wtType;

	@Column (name="note")
	private String note;
	
	//integration
	@ManyToOne
	@JoinColumn(name="brand_fk")
	private Brand brand;
	
	@ManyToOne
	@JoinColumn(name="category_fk")
	private Category category;
	
}

