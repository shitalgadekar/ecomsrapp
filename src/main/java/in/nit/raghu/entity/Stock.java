package in.nit.raghu.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name="stock_tab")
@NoArgsConstructor
@AllArgsConstructor
public class Stock {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="stockid")
	private Long id;
    
    @Column(name="stockqoh")
	private Long qoh;
    
    @Column(name="stocksold")
	private Long sold;
	
    //integration
	@OneToOne
	@JoinColumn(name="prodct_id_fk")
	private Product product;

	@Transient
	private Long count;


}
