package in.nit.raghu.entity;

import java.lang.Long;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "brand_tab")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Brand {
	
	@Id // pk
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name="brandname")
	private String name;
	
	@Column(name="brandcode")
	private String code;
	
	@Column (name="tagline")
	private String tagline;
	
	@Column (name="imagelink")
	private String imageLink;
	
	@Column(name="brandnote")
	private String note;

}
