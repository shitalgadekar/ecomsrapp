package in.nit.raghu.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name="shipping_tab")
@AllArgsConstructor
@NoArgsConstructor
public class Shipping {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@Column(name="shiptype")
	private String shipType;
	
	@Column(name="shipcode")
	private String shipCode;
	
	@Column(name="shipname")
	private String shipName;
	
	@Column(name="shipcost")
	private String shipCost;
	
	@Column(name="shipweight")
	private String shipWeight;
	
	@Column(name="shipweighttype")
	private String shipWeightType;
	
	@Column(name="shipnote")
	private String note;
	


}
