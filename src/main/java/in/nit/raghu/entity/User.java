package in.nit.raghu.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import in.nit.raghu.constants.UserRole;
import lombok.Data;

@Data
@Entity
@Table(name="user_tab")
public class User {
   
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="userid")
	private Long id;
	
	@Column(
			name="username",
			length = 45,
			nullable = false
			)
	private String displayName;
	
	@Column(
			name="useremail",
			length = 75,
			nullable = false,
			unique = true
			)
	private String email;
	
	@Column(
			name="userpwd",
			length = 120,
			nullable = false,
			unique = true
			)
	private String password;
	
	@Column(
			name="userstatus",
			nullable = false
			)
	private String status;
	
	@Column(name="usercontact",
			nullable = false
			)
	private String contact;
	
	@Column(name="userrole",
			nullable = false
			)
	@Enumerated(EnumType.STRING)
	private UserRole role;
	
	@Column(name="useraddress",
			nullable = false
			)
	private String address;

}
