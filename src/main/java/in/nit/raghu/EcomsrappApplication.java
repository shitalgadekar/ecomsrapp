package in.nit.raghu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcomsrappApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcomsrappApplication.class, args);
	}

}
