package in.nit.raghu.constants;

public enum UserRole {
	ADMIN, SALES, EMPLOYEE, USER
}
