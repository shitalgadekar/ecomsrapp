package in.nit.raghu.controller;

import in.nit.raghu.entity.Product;
import in.nit.raghu.exception.ProductNotFoundException;
import in.nit.raghu.service.IBrandService;
import in.nit.raghu.service.ICategoryService;
import in.nit.raghu.service.IProductService;
import java.lang.Long;
import java.lang.String;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/product")
public class ProductController {
	@Autowired
	private IProductService service;
	
	//for integration of brand and category
	@Autowired
	private IBrandService brandService;
	
	@Autowired
	private ICategoryService categoryService;
	
	private void commonUi(Model model) {
		model.addAttribute("brands", brandService.getBrandIdAndName());
		model.addAttribute("categories", categoryService.getCategoryIdAndName("ACTIVE"));
	}

	

	@GetMapping("/register")
	public String registerProduct(Model model) {
		commonUi(model);
		return "ProductRegister";
	}

	@PostMapping("/save")
	public String saveProduct(@ModelAttribute Product product, Model model) {
		java.lang.Long id=service.saveProduct(product);
		model.addAttribute("message","Product created with Id:"+id);
		commonUi(model);
		return "ProductRegister";
	}

	@GetMapping("/all")
	public String getAllProducts(Model model,
			@RequestParam(value = "message", required = false) String message) {
		List<Product> list=service.getAllProducts();
		model.addAttribute("list",list);
		model.addAttribute("message",message);
		return "ProductData";
	}

	@GetMapping("/delete")
	public String deleteProduct(@RequestParam Long id, RedirectAttributes attributes) {
		try {
			service.deleteProduct(id);
			attributes.addAttribute("message","Product deleted with Id:"+id);
		} catch(ProductNotFoundException e) {
			e.printStackTrace() ;
			attributes.addAttribute("message",e.getMessage());
		}
		return "redirect:all";
	}

	@GetMapping("/edit")
	public String editProduct(@RequestParam Long id, Model model, RedirectAttributes attributes) {
		String page=null;
		try {
			Product ob=service.getOneProduct(id);
			model.addAttribute("product",ob);
			page="ProductEdit";
			commonUi(model);
		} catch(ProductNotFoundException e) {
			e.printStackTrace() ;
			attributes.addAttribute("message",e.getMessage());
			page="redirect:all";
		}
		return page;
	}

	@PostMapping("/update")
	public String updateProduct(@ModelAttribute Product product, RedirectAttributes attributes) {
		service.updateProduct(product);
		attributes.addAttribute("message","Product updated");
		return "redirect:all";
	}
}
