package in.nit.raghu.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import in.nit.raghu.entity.Coupon;
import in.nit.raghu.exception.CouponNotFoundException;
import in.nit.raghu.service.ICouponService;

@Controller
@RequestMapping("/coupon")
public class CouponController {
	
	@Autowired
	private ICouponService service;
	
	@GetMapping("/register")
	public String registerCoupon(Model model) {
		model.addAttribute("message",new Coupon());
		return "CouponRegister";
	}
	
	@PostMapping("/save")
	public String saveCoupon(@ModelAttribute Coupon coupon,Model model) {
		Long id=service.saveCoupon(coupon);
		model.addAttribute("message","Coupon created with Id: "+id);
		model.addAttribute("coupon",new Coupon());
		return "CouponRegister";
	}
	
    @GetMapping("/all")
    public String getAllCoupons(Model model,@RequestParam(value="message" ,required=false) String message) {
    	List<Coupon> list=service.getAllCoupons();
    	model.addAttribute("list",list);
    	model.addAttribute("message",message);
    	return "CouponData";
    }
    
    @GetMapping("/delete")
    public String deleteById(@RequestParam Long id,RedirectAttributes attribute) {
    	try {
    		service.deleteCoupon(id);
    		attribute.addAttribute("message","Coupon deleted with Id: "+id);
    		
    	}catch(CouponNotFoundException e) {
    		e.printStackTrace();
    		attribute.addAttribute("message",e.getMessage());
    	}
    	
    	return "redirect:all";
    }
    
    @GetMapping("/edit")
    public String editCoupon(@RequestParam Long id,RedirectAttributes attribute,Model model) {
    	String page=null;
    	try {
    		Coupon ob=service.getOneCoupon(id);
    		attribute.addAttribute("coupon",ob);
    		page="CouponEdit";
    	}catch(CouponNotFoundException e) {
    		e.printStackTrace();
    		attribute.addAttribute("coupon",e.getMessage());
    		page="redirect:all";
    	}
    	return page;
    }
     
    @PostMapping("/update")
    public String updateCoupon(@ModelAttribute Coupon coupon,RedirectAttributes attribute)
    {
    	service.updateCoupon(coupon);
    	attribute.addAttribute("message","Coupon updated");
    	return "redirect:all";
    }
    
}
