package in.nit.raghu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import in.nit.raghu.entity.AppUser;
import in.nit.raghu.service.IAppUserService;

@Controller
@RequestMapping("/appuser")
public class AppUserController {

	@Autowired
	private IAppUserService service;
	
	@GetMapping("/register")
	public String appUserRegister() {
		return "AppUserRegister";
	}
	
	@PostMapping("/save")
	public String saveAppUser(
			@ModelAttribute AppUser user,Model model) {
		Long id=service.saveAppUser(user);
		model.addAttribute("message","AppUser" +id+ " Created");
		return "AppUserRegister";
	}
	
	@GetMapping("/all")
	public String showData(Model model) {
		model.addAttribute("list",service.getAllAppUser());
		return "AppUserData";
	}
}
