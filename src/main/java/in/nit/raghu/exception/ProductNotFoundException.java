package in.nit.raghu.exception;

import java.lang.RuntimeException;
import java.lang.String;

public class ProductNotFoundException extends RuntimeException {
  
	private static final long serialVersionUID = 1L;

    public ProductNotFoundException() {
    }

    public ProductNotFoundException(String message) {
      super(message);
    }
}
