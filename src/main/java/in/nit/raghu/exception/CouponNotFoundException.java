package in.nit.raghu.exception;

public class CouponNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public CouponNotFoundException() {
		super();
	}

	public CouponNotFoundException(String message) {
		super(message);
	}
}
