package in.nit.raghu.exception;

import java.lang.RuntimeException;
import java.lang.String;

public class BrandNotFoundException extends RuntimeException {
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    public BrandNotFoundException() {
    }

    public BrandNotFoundException(String message) {
       super(message);
    }
}
