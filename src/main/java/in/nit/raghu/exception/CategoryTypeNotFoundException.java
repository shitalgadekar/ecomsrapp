package in.nit.raghu.exception;

public class CategoryTypeNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	//private static final long serialVersionUID = 1L;
	private static final long serialVersionUID = 2344360133976960706L;

	public CategoryTypeNotFoundException() {
	}

	public CategoryTypeNotFoundException(String message) {
		super(message);
	}


}

